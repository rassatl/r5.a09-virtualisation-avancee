# Utilisez l'image PHP 8.1 avec Apache
FROM php:8.1-apache

# Créez et définissez le répertoire de travail
WORKDIR /var/www/html

# Copiez les fichiers de votre application dans le conteneur
COPY . /var/www/html

# Copiez le fichier de configuration Apache
COPY ./web/etc/apache.conf /etc/apache2/sites-enabled/000-default.conf

# Permettre à Composer de s'exécuter en tant qu'utilisateur root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Installez les extensions PHP requises, y compris GD
RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libzip-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo pdo_mysql

# Installez les dépendances système nécessaires
RUN apt-get update && apt-get install -y \
    git \
    zip \
    unzip

# Installez Composer globalement dans le conteneur
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Installez les dépendances PHP avec Composer
RUN composer install

# Exposez le port 81 pour Apache
EXPOSE 81

# Commande pour exécuter Apache
CMD ["apache2-foreground"]
